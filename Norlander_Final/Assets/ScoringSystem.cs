﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
public class ScoringSystem : MonoBehaviour 
{
    public GameObject scoreText;
    public int theScore;
    public AudioSource collectSound;
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        collectSound.Play();
        theScore += 1;
        scoreText.GetComponent<Text>().text = "Clues: " + theScore;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
